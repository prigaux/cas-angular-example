<?php
require_once 'CAS.php';
$CAS_HOST = 'cru.renater.fr';
$CAS_CONTEXT = '/cas';
ini_set('session.use_cookies', 0);
// because of https://bugs.php.net/bug.php?id=72915 , we can't use "Authorization"
if (isset($_SERVER['HTTP_X_AUTHORIZATION'])) {
    if (preg_match("/Bearer\s+(\S+)/", $_SERVER['HTTP_X_AUTHORIZATION'], $m)) {
        //error_log("got bearer $m[1]");
        session_id($m[1]);
        session_start();
    }
}

phpCAS::client(CAS_VERSION_2_0, $CAS_HOST, 443, $CAS_CONTEXT);
phpCAS::setNoCasServerValidation();
